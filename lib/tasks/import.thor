require File.expand_path('config/environment.rb')

class Import < Thor
  desc "files [filenames]", "import customer list files (including remote URI's)"
  def files(*filenames)
    raise ArgumentError, "You need to specify at least one filepath or URI." unless filenames.present?
    filenames.each { |filename| CustomerFile.new(filename).import }
  end
end
