RSpec.shared_context "pompano honey", :a => :b do
  let(:customer) { Customer.create!(first_name: "Jimmy", last_name: "McNulty", email: "thewire@hbo.com") }
  let(:vehicle_type) { VehicleType.create!(name: "Catamaran") }
  let(:vehicle) { Vehicle.create!(name: "Pompano Honey", length_in_feet: 46, vehicle_type: vehicle_type, customer: customer) }
end
