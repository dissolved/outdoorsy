RSpec.describe VehiclesController, type: :routing do
  include_context "pompano honey"

  before { vehicle }
  describe "routing" do
    it "routes to #index" do
      expect(get: "/vehicles").to route_to("vehicles#index")
    end

    it "routes to #show" do
      expect(get: "/vehicles/1").to route_to("vehicles#show", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/vehicles/1").to route_to("vehicles#destroy", id: "1")
    end
  end
end
