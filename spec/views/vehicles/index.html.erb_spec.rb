RSpec.describe "vehicles/index", type: :view do
  include_context "pompano honey"

  before(:each) do
    assign(:vehicles, [
      Vehicle.create!(
        name: "Boaty Boat",
        length_in_feet: 25,
        vehicle_type: vehicle_type,
        customer: customer
      ),
      Vehicle.create!(
        name: "Rusty Boat",
        length_in_feet: 28,
        vehicle_type: vehicle_type,
        customer: customer
      )
    ])
  end

  it "renders a list of vehicles" do
    render
    assert_select "tr>td", text: "Boaty Boat"
    assert_select "tr>td", text: "Rusty Boat"
    assert_select "tr>td", text: "25"
    assert_select "tr>td", text: "28"
    assert_select "tr>td", text: customer.full_name, count: 2
  end
end
