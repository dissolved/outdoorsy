RSpec.describe "vehicles/show", type: :view do
  include_context "pompano honey"

  before(:each) do
    @vehicle = assign(:vehicle, vehicle)
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/#{vehicle.name}/)
    expect(rendered).to match(/#{vehicle.length_in_feet}/)
    expect(rendered).to match(/#{vehicle.vehicle_type_name}/)
    expect(rendered).to match(/#{vehicle.customer_full_name}/)
  end
end
