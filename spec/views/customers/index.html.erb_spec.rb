require 'rails_helper'

RSpec.describe "customers/index", type: :view do
  before(:each) do
    assign(:customers, [
      Customer.create!(
        first_name: "First",
        last_name: "Last",
        email: "valid@email.com"
      ),
      Customer.create!(
        first_name: "Otherfirst",
        last_name: "Otherlast",
        email: "other@valid.email"
      )
    ])
  end

  it "renders a list of customers" do
    render
    assert_select "tr>td", text: "First Last".to_s
    assert_select "tr>td", text: "Otherfirst Otherlast".to_s
    assert_select "tr>td", text: "valid@email.com".to_s
    assert_select "tr>td", text: "other@valid.email".to_s
  end
end
