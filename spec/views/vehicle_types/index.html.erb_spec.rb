RSpec.describe "vehicle_types/index", type: :view do
  before(:each) do
    assign(:vehicle_types, [
      VehicleType.create!(
        name: "Boat"
      ),
      VehicleType.create!(
        name: "Train"
      )
    ])
  end

  it "renders a list of vehicle_types" do
    render
    assert_select "tr>td", text: "Boat"
    assert_select "tr>td", text: "Train"
  end
end
