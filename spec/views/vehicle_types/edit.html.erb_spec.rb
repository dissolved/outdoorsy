require 'rails_helper'

RSpec.describe "vehicle_types/edit", type: :view do
  before(:each) do
    @vehicle_type = assign(:vehicle_type, VehicleType.create!(
      name: "MyString"
    ))
  end

  it "renders the edit vehicle_type form" do
    render

    assert_select "form[action=?][method=?]", vehicle_type_path(@vehicle_type), "post" do

      assert_select "input[name=?]", "vehicle_type[name]"
    end
  end
end
