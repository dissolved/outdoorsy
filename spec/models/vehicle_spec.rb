RSpec.describe Vehicle, type: :model do
  include_context "pompano honey"

  it "delegates customer_full_name to associated customer" do
    expect(vehicle.customer_full_name).to eq(customer.full_name)
  end

  it "delegates vehicle_type_name to associated vehicle_type" do
    expect(vehicle.vehicle_type_name).to eq(vehicle_type.name)
  end
end
