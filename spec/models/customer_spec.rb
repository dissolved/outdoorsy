RSpec.describe Customer, type: :model do
  include_context "pompano honey"

  it "concatenates first and last name" do
    expect(customer.full_name).to eq("Jimmy McNulty")
  end
end
