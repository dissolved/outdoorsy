describe CustomerFile do
  let(:file_uri) { file_fixture("commas.txt").to_s }

  it "requires an argument" do
    expect { CustomerFile.new }.to raise_error(ArgumentError)
    expect { CustomerFile.new(file_uri) }.not_to raise_error
  end

  describe "import" do
    subject { described_class.new(file_uri).import }

    it "reads comma separated data into customers table" do
      expect { subject }.to change { Customer.count }.by(4)
    end

    it "reads comma separated data into vehicles table" do
      expect { subject }.to change { Vehicle.count }.by(4)
    end

    context "with pipes as delimiter" do
      let(:file_uri) { file_fixture("pipes.txt").to_s }

      it "reads pipes separated data" do
        expect { subject }.to change { Customer.count }.by(4)
      end

      it "populates the vehicle_types table" do
        expect { subject }.to change { VehicleType.count }.by(4)
      end
    end
  end
end
