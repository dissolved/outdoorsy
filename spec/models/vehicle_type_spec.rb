RSpec.describe VehicleType, type: :model do
  include_context "pompano honey"

  before { vehicle_type }
  it "prevents duplicate types" do
    expect { VehicleType.create!(name: vehicle_type.name) }.to(
      raise_error(ActiveRecord::RecordInvalid)
    )
  end
end
