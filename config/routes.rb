Rails.application.routes.draw do
  root 'vehicles#index'
  resources :vehicles, only: %i[show index destroy]
  resources :vehicle_types, except: %i[destroy]
  resources :customers
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
