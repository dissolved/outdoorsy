json.extract! vehicle, :id, :name, :vehicle_type_id, :customer_id, :created_at, :updated_at
json.url vehicle_url(vehicle, format: :json)
