require 'csv'
require 'open-uri'

class CustomerFile
  attr_accessor :file_uri

  def initialize(file_uri)
    # file_uri can be either a filepath or a well formed
    # uri. Basically, if open-uri can open it, then it
    # is a valid argument
    self.file_uri = file_uri
  end

  def import
    # CSV.foreach(file_uri) do |row|
    csv = CSV.new(file_data, col_sep: delimeter, headers: HEADERS)
    csv.each do |row|
      customer = Customer.find_or_create_by!(**row.to_h.slice(*CUSTOMER_ATTRS))
      vehicle_type = VehicleType.find_or_create_by!(name: row[:vehicle_type])
      vehicle = create_vehicle(
        customer: customer,
        vehicle_type: vehicle_type,
        **row.to_h.slice(*VEHICLE_ATTRS)
      )
    rescue StandardError => e
      puts "Dropping row due to #{e.inspect}\n#{row.inspect}"
    end
  end

  private

    CUSTOMER_ATTRS = %i[first_name last_name email]
    VEHICLE_ATTRS = %i[name length_str]
    HEADERS = CUSTOMER_ATTRS + [:vehicle_type] + VEHICLE_ATTRS

    UNICODE_CONVERSIONS = {
      /[’`´‘]/ => "'",
      /[“”]/ => '"',
    }

    def create_vehicle(name:, length_str:, customer:, vehicle_type:)
      customer.vehicles.find_or_create_by!(
        name: name,
        length_in_feet: to_feet(length_str),
        vehicle_type: vehicle_type
      )
    end

    def delimeter
      # this is a quick and dirty (ie non-production worthy)
      # helper method to sniff the delimiter. There is an a
      # gem that hasn't been maintained in years that
      # probably still works ok, named csv_sniffer
      [',', '\t', ';', '|'].max do |a, b|
        file_data.count(a) <=> file_data.count(b)
      end
    end

    def file_data
      # memoize the file contents since we will be using it
      # multiple times
      @file_data ||= open(file_uri) { |f| f.read }
    end

    def to_feet(str)
      UNICODE_CONVERSIONS.each do |matcher, target_char|
        str.gsub!(matcher, target_char)
      end
      Measurement.parse(str).convert_to(:feet).quantity.round
    end
end
