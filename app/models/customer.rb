class Customer < ApplicationRecord
  has_many :vehicles
  has_many :vehicle_types, through: :vehicles

  scope :by_last_name, -> { order(:last_name) }

  validates :email, allow_blank: true, format: {
    with: /\A[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\z/i
  }

  def full_name
    [first_name, last_name].compact.join(" ")
  end
end
