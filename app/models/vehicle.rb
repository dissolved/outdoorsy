class Vehicle < ApplicationRecord
  belongs_to :vehicle_type
  belongs_to :customer

  scope :sorted, ->(by) { eager_load(:vehicle_type, :customer).order(lower(by)) }

  validates :length_in_feet, numericality: true

  delegate :name, to: :vehicle_type, prefix: true, allow_nil: true
  delegate :full_name, to: :customer, prefix: true, allow_nil: true

  class << self
    def lower(query)
      # I hate doing this, in production I'd solve this, but time to submit this
      # exercise already!
      query == "length_in_feet" ? query : Arel.sql("LOWER(#{query})")
    end
  end
end
