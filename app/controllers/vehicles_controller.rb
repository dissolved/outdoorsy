class VehiclesController < ApplicationController
  before_action :set_vehicle, only: %i[ show edit update destroy ]

  # GET /vehicles or /vehicles.json
  def index
    @vehicles = Vehicle.sorted(sort_by).all
  end

  # GET /vehicles/1 or /vehicles/1.json
  def show
  end

  # DELETE /vehicles/1 or /vehicles/1.json
  def destroy
    @vehicle.destroy
    respond_to do |format|
      format.html { redirect_to vehicles_url, notice: "Vehicle was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def vehicle_params
      params.require(:vehicle).permit(:name, :length_in_feet, :vehicle_type_id, :customer_id)
    end

    def sort_by
      PERMITTED_SORT_VALUES.include?(params[:sort_by]) ? params[:sort_by] : "vehicles.name"
    end

    PERMITTED_SORT_VALUES = %w[vehicles.name length_in_feet vehicle_types.name customers.last_name]
end
