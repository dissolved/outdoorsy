class CreateVehicleTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :vehicle_types do |t|
      t.string :name, index: true

      t.timestamps
    end
  end
end
