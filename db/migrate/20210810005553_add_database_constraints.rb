class AddDatabaseConstraints < ActiveRecord::Migration[6.1]
  def change
    change_column_null :vehicle_types, :name, false
    change_column_null :customers, :last_name, false
  end
end
