# Outdoorsy

## Installing
(built with Ruby 2.7.4)

- `git clone https://gitlab.com/dissolved/outdoorsy.git`
- `cd outdoorsy`
- `bundle install`
- `bin/rails db:migrate`

## Running Tests
`rspec spec`

## Importing Sample Data
`thor import:files spec/fixtures/files/pipes.txt spec/fixtures/files/commas.txt`

## Running server locally
`bin/rails s`

## Deploying to Heroku
- `heroku login`
- `heroku create`
- `git push heroku main`
- `heroku run rake db:migrate`
- `heroku run thor import:files spec/fixtures/files/pipes.txt spec/fixtures/files/commas.txt`

## Database
For ease of running, the development database is sqlite though the app has been configured to use PostgreSQL in production. This is a design decision I feel is appropriate for an exercise like this, but in a real production app I'd choose to use the same database in development as production.

## High Level Code Overiew

In general, I tried to make my commits in logical groups of work, and whenever code was generated for me, I tried to commit that separately so that reviewers can easily distinguish between the code I wrote and the code that was generated.

### File Importing
The bulk of this part of the exercise is split between 2 files. There is a Thor task (named `import.thor`) that simply reads filepath (or you can provide any valid URI), and instantiates an instance of class named `CustomerFile` then calls `import` on it.

### Models
Just 3 simple ActiveRecord models.
- Customer
- Vehicle
- VehicleType

### Sorting Data
On the main page of the app (an alias to the `/vehicles` endpoint), you can sort the rows by any column. Just click on the column name.

### Everything else
While I touched much of the view/controller code, the bulk of that was code generated, and not really in scope of the exercise anyway.
